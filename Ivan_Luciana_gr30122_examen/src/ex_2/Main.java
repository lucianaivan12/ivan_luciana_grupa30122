package ex_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class Main {

	public static void main(String[] args) {
		
		new Win();
	}
}

class Win extends JFrame {
	

	JTextArea ta;
	JButton b;
	
	Win(){
		
		setTitle("Exam"); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		ta= new JTextArea("");
		ta.setBounds(20, 60, 350, 200);
		add(ta);
		b = new JButton("Save");
		b.setBounds(140, 280, 100, 50);
		add(b);
		b.addActionListener(new AL());
		
		setSize(400,400);
		setVisible(true);
		
	}
	

	class AL implements ActionListener{
		
		void save(String f)
	      {
	            try
	            {
	            PrintWriter pwriter = new PrintWriter(
	                  new FileWriter(new File(f)));
	            pwriter.println(ta.getText());
	            pwriter.close();
	            } catch (IOException e) {
	          System.out.println("Error!");
	          e.printStackTrace();
	        }
	 
	      }

		@Override
		public void actionPerformed(ActionEvent e) {
			
			String name= "Exam.txt";
			save(name);
		}
	
	
 }
}