package ex_1;

public class G {
	
	public static void main(String[] args) {

	}

	public void i(){
		
	}
}

class B extends G{
	
	private long t;
	
	public void x( D d){
		
	}
}

class D{
	
}

interface I{
	
}

class H{
	
}

class F{
	
	public void metA() {
		
	}
}

class E implements I{
	
	B b;
	F f;
	H h;
	
	E(){
		new B();	
	}
	
}